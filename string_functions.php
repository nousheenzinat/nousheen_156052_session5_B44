<?php

echo "how are \"you\"";
echo "<br>";

$myStr=addslashes(' Hello \37\ Hello "37" Hello \'37\' Hello "37" Hello "37"');
//except null all string functons used here


echo "$myStr";

//explode break a string into array
$myStr="hello world ! how is life ?"."<br>";

$myArr = explode(" ",$myStr);
print_r($myArr);
echo "<br>";


//implode break a array into string


$myStr = implode("*",$myArr);
echo $myStr;

$myStr= "<br> means line  break";
$myStr=htmlentities($myStr);
echo $myStr ;



//trim
$myStr= "                         hello world";
echo ltrim($myStr);



$myStr= "         hello world";
echo rtrim($myStr);

//new line

$myStr = "\n\n\n\n\n hello";
echo nl2br($myStr);//nl2br used to convert br into new line
echo"<br>";



//str_pad

$mainstr = "hello world";

$padstr = str_pad($mainstr,50,"*");
echo $padstr;
echo "<br>";


//str_repeat

$mainstr = "hello world";

$repeatstr = str_repeat($mainstr,5);
echo $repeatstr;
echo "<br>";

//replace

$mainstr = "hello world";

$replacestr = str_replace("o","O",$mainstr);
echo $replacestr;
echo "<br>";

//str_split

$mainstr = "hello world";

$myArr = str_split($mainstr);
print_r($myArr);
echo "<br>";

//str_length
$mainstr = "hello world";
echo strlen($mainstr);

//startupper
$mainstr = "hello world";
echo strtoupper($mainstr);
echo "<br>";
//start lower



//substring
$mainstr = "hello world";
$substr ="hell";
echo substr_compare($mainstr,$substr,0);
echo "<br>";

//str_count
$mainstr = "hello world";
echo substr_count($mainstr,"world");
echo "<br>";

//substr_replace

$mainstr = "hello world";
$substr = "hell";
echo substr_replace($mainstr,$substr,10);

//ucfirst
$mainstr = "hello world";
echo ucfirst($mainstr);
echo "<br>";
echo ucwords($mainstr);

?>

